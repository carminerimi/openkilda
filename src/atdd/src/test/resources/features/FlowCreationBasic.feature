Feature: Basic Flow Creation

  Scenario: Small Linear Network, Full Flow

    This scenario will setup a flow across the entire set of switches

    Given BasicFlowGiven_TBD
    When BasicFlowWhen_TBD
    Then BasicFlowThen_TBD
