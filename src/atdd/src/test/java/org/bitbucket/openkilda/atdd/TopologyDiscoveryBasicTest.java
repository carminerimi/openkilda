package org.bitbucket.openkilda.atdd;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TopologyDiscoveryBasicTest {

	public TopologyDiscoveryBasicTest() {
	}

	@Given("^a new controller$")
	public void a_new_controller() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^a random linear topology of (\\d+)$")
	public void a_random_linear_topology_of(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
			// TODO: Will need to send the IP address of the controller.
	    throw new PendingException();
	}

	@When("^the controller learns the topology$")
	public void the_controller_learns_the_topology() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^the controller should converge within (\\d+) milliseconds$")
	public void the_controller_should_converge_within_milliseconds(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^a random full-mesh topology of (\\d+)$")
	public void a_random_full_mesh_topology_of(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

}
